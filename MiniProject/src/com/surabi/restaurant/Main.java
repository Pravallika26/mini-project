package com.surabi.restaurant;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Date time = new Date();
		List<BillingSystem> bills = new ArrayList<BillingSystem>();
		boolean finalOrder;
		List<RestaurantMenu> Menu = new ArrayList<RestaurantMenu>();
		
        //creating objects 
		RestaurantMenu dish1 = new RestaurantMenu(1, "veg fried rice", 2, 150.0);       //passing parameters
		RestaurantMenu dish2 = new RestaurantMenu(2, "Momos", 2, 190.0);
		RestaurantMenu dish3 = new RestaurantMenu(3, "chilli chicken", 2, 180.0);
		RestaurantMenu dish4 = new RestaurantMenu(4, "Hyderabadi biryani", 2, 190.0);
		RestaurantMenu dish5 = new RestaurantMenu(5, "Chilli Potato", 1, 250.0);

		//adding dishes to list
		Menu.add(dish1);
		Menu.add(dish2);
		Menu.add(dish3);
		Menu.add(dish4);
		Menu.add(dish5);
		
		//validations for dishes
		dish1.validate(dish1);
		dish2.validate(dish2);
		dish3.validate(dish3);
		dish4.validate(dish4);
		dish5.validate(dish5);

		System.out.println(
				"+=+=+=+=+ Welcome to Surabi Restaurants +=+=+=+=+ \n========================================================");
		while (true) {
			System.out.println("Please enter your credentials");

			System.out.println("Email : ");
			String email = scan.next();

			System.out.println("Password : ");
			String password = scan.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please enter A if you are Admin and U if you are User");
			String aorU = scan.next();

			BillingSystem bill = new BillingSystem();
			List<RestaurantMenu> selectedMenu = new ArrayList<RestaurantMenu>();

			double totalCost = 0;

			Date date = new Date();

			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH : mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (aorU.equals("U") || aorU.equals("u")) {
				System.out.println("Welcome User " + name);
				do {
					System.out.println("Menu for today : ");
					Menu.stream().forEach(d -> System.out.println(d));
					System.out.println("Enter the Menu code");
					int code = scan.nextInt();

					if (code == 1) {
						selectedMenu.add(dish1);
						totalCost += dish1.getItemPrice();
					} else if (code == 2) {
						selectedMenu.add(dish2);
						totalCost += dish2.getItemPrice();
					} else if (code == 3) {
						selectedMenu.add(dish3);
						totalCost += dish3.getItemPrice();
					} else if (code == 4) {
						selectedMenu.add(dish4);
						totalCost += dish4.getItemPrice();
					} else {
						selectedMenu.add(dish5);
						totalCost += dish5.getItemPrice();
					}

					System.out.println("Press 0 to show bill\nPress 1 to order more");

					int opt = scan.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);

				System.out.println("Thank you " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedMenu.stream().forEach(order -> System.out.println(order));
				System.out.println("Your total bill is : " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setDishes(selectedMenu);
				bill.setTime(date);
				bills.add(bill);

			} else if (aorU.equals("A") || aorU.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = scan.nextInt();
				switch (option) {
				case 1:
					if (!bills.isEmpty()) {
						for (BillingSystem b : bills) {
							if (b.getTime().getDate() == time.getDate()) {
								System.out.println("Username : " + b.getName());
								System.out.println("Items : " + b.getDishes());
								System.out.println("Total : " + b.getCost());
								System.out.println("Date : " + b.getTime());
							}
						}
					} else
						System.out.println("No Bills for today...!!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (BillingSystem b : bills) {
							if (b.getTime().getMonth() == time.getMonth()) {
								System.out.println("Username : " + b.getName());
								System.out.println("Items : " + b.getDishes());
								System.out.println("Total : " + b.getCost());
								System.out.println("Date : " + b.getTime());
							}
						}
					} else
						System.out.println("No Bills for this month...!!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (BillingSystem b : bills) {
							System.out.println("\nUsername : " + b.getName());
							System.out.println("Items : " + b.getDishes());
							System.out.println("Total : " + b.getCost());
							System.out.println("Date : " + b.getTime());
						}
					} else
						System.out.println("No Bills...!!");

					break;

				default:
					System.out.println("Invalid option");
					System.exit(1);
				}
			} else if (aorU.equals("E") || aorU.equals("e")) {
				System.exit(1);

			} else {
				System.out.println("Invalid option..check again");
			}

		}

	}
}
