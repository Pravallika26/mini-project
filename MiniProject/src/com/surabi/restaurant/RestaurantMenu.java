package com.surabi.restaurant;

public class RestaurantMenu {
	private int itemID;
	private String itemName;
	private int itemQuantity;
	private double itemPrice;

	public RestaurantMenu(int itemID, String itemName, int itemQuantity, double itemPrice)
			throws IllegalArgumentException {
		super();

		this.itemID = itemID;
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.itemPrice = itemPrice;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "RestaurantMenu [ItemID=" + itemID + ", ItemName=" + itemName + ", ItemQuantity=" + itemQuantity
				+ ", ItemPrice=" + itemPrice + "]";
	}
	public void validate(RestaurantMenu dishes) {
		if(dishes.getItemID()>0 && dishes.getItemName()!=null && !( dishes.getItemName().isEmpty()) && dishes.getItemQuantity()>0  && dishes.getItemPrice() != 0) {
			return ;
		}
		else {
			throw new IllegalArgumentException("Invalid entry...check again");
		}
	}
	}


