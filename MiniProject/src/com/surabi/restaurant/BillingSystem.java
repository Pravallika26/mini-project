package com.surabi.restaurant;

import java.util.Date;
import java.util.List;

public class BillingSystem {
	private String name;
	private List<RestaurantMenu> dishes;
	private double cost;
	private Date time;

	public BillingSystem() {
	}

	public BillingSystem(String name, List<RestaurantMenu> items, double cost, Date time)
			throws IllegalArgumentException {
		super();

		this.name = name;
		this.dishes = items;
		if (cost < 0) {
			throw new IllegalArgumentException("Cost cannot be less than zero...check again");
		}
		this.cost = cost;
		this.time = time;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RestaurantMenu> getDishes() {
		return dishes;
	}

	public void setDishes(List<RestaurantMenu> dishes) {
		this.dishes = dishes;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "BillingSystem [name=" + name + ", dishes=" + dishes + ", cost=" + cost + ", time=" + time + "]";
	}

}
